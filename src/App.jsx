import './App.css';
import Ex_Form from './Ex_Form/Ex_Form';

function App() {
  return (
    <div className="container py-5">
      <Ex_Form />
    </div>
  );
}

export default App;
