import React, { Component } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { SUA, XOA } from './redux/action/DanhSachNguoiDung'

const RenderDanhSachNguoiDung = () => {
    const dispatch = useDispatch()
    const data = useSelector(allState => allState.danhSachNguoiDung.data)
    return <>
        {
            data.map((item, index) => {
                let { id, maSV, hoTen, soDienThoai, email } = item
                return <tr key={index}>
                    <td>{id}</td>
                    <td>{maSV}</td>
                    <td>{hoTen}</td>
                    <td>{soDienThoai}</td>
                    <td>{email}</td>
                    <td>
                        <button
                            onClick={() => {
                                dispatch({
                                    type: SUA,
                                    payload: id
                                })
                            }}
                            className='btn btn-warning mr-2'>Sửa</button>
                        <button
                            onClick={() => {
                                dispatch({
                                    type: XOA,
                                    payload: id
                                })
                            }}
                            className='btn btn-danger'>Xoá</button>
                    </td>
                </tr>
            })
        }
    </>

}

export default function DanhSachNguoiDung() {
    return (
        <div>
            <table className='table mt-5'>
                <thead>
                    <td>ID</td>
                    <td>Mã SV</td>
                    <td>Họ Tên</td>
                    <td>Số Điện Thoại</td>
                    <td>Email</td>
                    <td>Thao Tác</td>
                </thead>
                <tbody>
                    <RenderDanhSachNguoiDung />
                </tbody>
            </table>
        </div>
    )

}
