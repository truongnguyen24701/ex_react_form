import React, { Component } from 'react'
import FormNguoiDung from './FormNguoiDung'
import shortid from 'shortid'
import DanhSachNguoiDung from './DanhSachNguoiDung'

export default class Ex_Form extends Component {
    render() {
        return (
            <div>
                <FormNguoiDung
                     />
                <DanhSachNguoiDung
                />
            </div>
        )
    }
}
