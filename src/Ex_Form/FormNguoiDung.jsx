import React, { Component } from 'react'
import { useState } from 'react'
import { initalUser } from './utils_ex_QuanLyNguoiDung'
import * as yup from 'yup'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { CAP_NHAP, THEM } from './redux/action/DanhSachNguoiDung'

const FormNguoiDung = () => {
    const dispatch = useDispatch()
    const userEdited = useSelector(allState => allState.danhSachNguoiDung.userEdited)

    const schema = yup.object({
        maSV: yup.string().required("Không được để trống"),
        hoTen: yup.string().required("Không dược để trống"),
        soDienThoai: yup.string().max(10, "Số điện thoại không quá 10 ký tự").required("Không được để trống"),
        email: yup.string().email("Email không định dạng").required("Không được để trống")

    })
    const form = useForm({
        defaultValues: {
            maSV: "",
            hoTen: "",
            soDienThoai: "",
            email: ""
        },
        resolver: yupResolver(schema),
    })

    useEffect(() => {
        if (userEdited) {
            form.reset({ ...userEdited })
        }
    }, [userEdited])

    const { handleSubmit, register, formState: { errors } } = form

    const onSubmit = (data) => {
        if (userEdited) {
            dispatch({
                type: CAP_NHAP,
                payload: data
            })
        } else {
            dispatch({
                type: THEM,
                payload: data
            })
        }
        form.reset({
            maSV: "",
            hoTen: "",
            soDienThoai: "",
            email: ""
        })
    }

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <div className='container py-5'>
                <h1>Thông tin sinh viên</h1>
                <div class="form-group">
                    <input
                        disabled={userEdited ? true : false}
                        type="text"
                        class="form-control"
                        placeholder="Mã SV"
                        {...register('maSV')}
                    />
                    <p>{errors?.maSV?.message}</p>
                </div>
                <div class="form-group">
                    <input
                        type="text"
                        class="form-control"
                        placeholder="Họ Tên"
                        {...register('hoTen')}
                    />
                    <p>{errors?.hoTen?.message}</p>
                </div>
                <div class="form-group">
                    <input
                        type="number"
                        class="form-control"
                        placeholder="Số Điện Thoại"
                        {...register('soDienThoai')}
                    />
                    <p>{errors?.soDienThoai?.message}</p>
                </div>
                <div class="form-group">
                    <input
                        type="text"
                        class="form-control"
                        name="email"
                        placeholder="Email"
                        {...register('email')}
                    />
                    <p>{errors?.email?.message}</p>
                </div>
                <button
                    className='btn btn-primary'>{userEdited ? 'Cập Nhập Sinh Viên' : 'Thêm Sinh Viên'}</button>
            </div>
        </form>
    )
}

export default FormNguoiDung
