import { CAP_NHAP, SUA, THEM, XOA } from "../action/DanhSachNguoiDung";
import shortid from 'shortid'

export default function (state = {
    data: [],
    userEdited: null
}, action) {
    switch (action.type) {
        case THEM: {
            let newUser = { ...action.payload, id: shortid.generate() }
            return {
                ...state,
                data: [...state.data, newUser]
            }
        }
        case XOA: {
            let index = state.findIndex((user) => {
                return user.id == action.payload
            })
            let cloneDanhSachNguoiDung = [...state.data]
            if (index !== -1) {
                cloneDanhSachNguoiDung.splice(index, 1)
            }
            return cloneDanhSachNguoiDung
        }
        case CAP_NHAP: {
            let index = state.data.findIndex((user) => {
                return user.maSV == action.payload.maSV
            })
            if (index !== -1) {
                let cloneDanhSachNguoiDung = [...state.data]
                cloneDanhSachNguoiDung[index] = action.payload

                return {
                    ...state,
                    data: cloneDanhSachNguoiDung,
                    userEdited: null
                }

            }
            return state
        }
        case SUA: {
            let index = state.data.findIndex((user) => {
                return user.id == action.payload
            })
            if (index !== -1) {
                return {
                    ...state,
                    userEdited: state.data[index]
                }
            }
            return state
        }
        default: return state;
    }
};
