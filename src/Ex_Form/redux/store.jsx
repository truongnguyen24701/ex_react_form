import { combineReducers,createStore } from 'redux'
import danhSachNguoiDung from './reducer/danhSachNguoiDung'

const rootReducer = combineReducers ({
    danhSachNguoiDung: danhSachNguoiDung,
})

export default createStore(rootReducer)
